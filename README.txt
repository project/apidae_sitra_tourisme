CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * Maintainers


 INTRODUCTION
 ------------

 The Apidae Sitra Tourisme module creates nodes based on your selections on apidae-tourisme.fr.
 You can add multiple selections and select the desired types of objects to retrieve.
 Automatic update (via Drupal cron) is available with two options :
    * Retrieve only new objects
    * Update already existing objects

 REQUIREMENTS
 ------------

 This module requires the following modules:
 * Composer Manager (https://www.drupal.org/project/composer_manager)
 * Date (https://www.drupal.org/project/date)

 INSTALLATION
 ------------

 Please read the following document : https://github.com/sitra-tourisme/sitra-api-php

 Once you've installed sitra-api-php, move the newly created 'vendor' folder in the folder 'sites/all'.
 Install composer manager module to load the autoload.php.

 CONFIGURATION
 -------------

 Go to the the administration page 'website/admin/config/systeme/tgb_apidae' and enter your API key, your id and your selections id.
 Check if you want to automatically update your content (via Drupal cron).

 Choose the type of data to retrieve.

 TROUBLESHOOTING
 ---------------

 Fields with long values (such as "Horaires") might cause trouble when editing a node, drupal will say that the value is too long.
 I haven't resolved this issue yet.

 The module can be improved, especially by extending the existing fields to all objects.
 Creating a "test" button in the administration page to check if the credentials entered are correct can be a good idea too.

 MAINTAINERS
 -----------

 Current maintainers:
 * Maxime Defrancq (Grezag) - https://www.drupal.org/u/grezag

 This project has been sponsored by:
 * The Gobeliners
   Web agency, webdesign and development.
   Visit http://www.thegobeliners.com/